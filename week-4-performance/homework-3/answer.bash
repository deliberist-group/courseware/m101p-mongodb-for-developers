#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader 'Copying zip file.'
ZIP_NAME=chapter_4_performance.06c44976aabe.zip
cp -v "../${ZIP_NAME}" .

printheader 'Unzipping blog directory.'
BLOG_DIR="hw4-3__m101p_mongodb-2.6_mongodb-3.0_pymongo-3.0_551e08e4d8ca3941fb77dfdb"
unzip "${ZIP_NAME}" "${BLOG_DIR}/*"

printheader 'Deleting zip file.'
rm -fv "${ZIP_NAME}"

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader 'Importing test data.'
mongoimport --drop -d blog -c posts "./${BLOG_DIR}/hw4-3/posts.json"

printheader "Creating indicies"
mongo blog <<EOF
    db.posts.createIndex({date:1})
    db.posts.createIndex({tags:1, date:-1})
    db.posts.createIndex({permalink:1})
EOF

printheader 'Validating... If this works the answer is the validate code!!!'
python "./${BLOG_DIR}/hw4-3/validate.py"

cleanupmongo

printheader 'Deleting blog directory.'
rm -rfv "${BLOG_DIR}"


