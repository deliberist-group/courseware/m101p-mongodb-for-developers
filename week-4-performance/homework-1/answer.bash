#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader "Creating indices."
mongo --quiet products <<EOF
    db.products.createIndex({'sku':1}, {'unique':true})
    db.products.createIndex({'price':-1})
    db.products.createIndex({'description':1})
    db.products.createIndex({'category':1, 'brand':1})
    db.products.createIndex({'reviews.author':1})
EOF

printheader "Checking answer option #1"
mongo --quiet products <<EOF
    db.products.explain().find( { 'brand' : 'GE' } )
EOF

printheader "Checking answer option #2"
mongo --quiet products <<EOF
    db.products.explain().find( { 'brand' : 'GE' } ).sort( { price : 1 } )
EOF

printheader "Checking answer option #3"
mongo --quiet products <<EOF
    db.products.explain().find( { \$and : [ { price : { \$gt : 30 } }, { price : { \$lt : 50 } } ] } ).sort( { brand : 1 } )
EOF

printheader "Checking answer option #4"
mongo --quiet products <<EOF
    db.products.explain().find( { brand : 'GE' } ).sort( { category : 1, brand : -1 } )
EOF

printheader "Finding the results."
cat <<EOF
You must manually check the results above because the document provided by the
explain() function cannot easily be queried.  For some queries there are 
multiple input stages that are nested inside each other.

The answer should be #2 and #3 can use IXSCAN.
EOF

cleanupmongo

