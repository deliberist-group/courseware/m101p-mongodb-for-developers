#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader 'Copying zip file.'
ZIP_NAME=chapter_5_aggregation_framework.f05059ebb3a5.zip
cp -v "../${ZIP_NAME}" .

printheader 'Unzipping posts data.'
POSTS_FILE="posts.json"
unzip "${ZIP_NAME}" "${POSTS_FILE}"

printheader 'Deleting zip file.'
rm -fv "${ZIP_NAME}"

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader 'Importing post data.'
BLOG_DB_NAME=blog
mongoimport --drop -d "${BLOG_DB_NAME}" -c posts "${POSTS_FILE}"

printheader 'Deleting post file.'
rm -rfv "./${POSTS_FILE}"

printheader "Finding the commenter with the least number of comments."
EXPECTED='{ "_id" : "Mariela Sherer", "num_of_comments" : 387 }'
ACTUAL=$(mongo --quiet "${BLOG_DB_NAME}" <<EOF
    db.posts.aggregate([ \
        {'\$unwind': '\$comments'}, \
        {'\$group': { \
           '_id': '\$comments.author', \
           num_of_comments: {'\$sum': 1} \
        }}, \
        {'\$sort': {num_of_comments: 1}}, \
        {'\$limit': 1} \
    ])
EOF
)
assertequals "${EXPECTED}" "${ACTUAL}" "Least commenter names do not match"

printheader "Finding the commenter with the MOST number of comments."
ANSWER=$(mongo --quiet "${BLOG_DB_NAME}" <<EOF
    db.posts.aggregate([ \
        {'\$unwind': '\$comments'}, \
        {'\$group': { \
           '_id': '\$comments.author', \
           num_of_comments: {'\$sum': 1} \
        }}, \
        {'\$sort': {num_of_comments: -1}}, \
        {'\$limit': 1} \
    ])
EOF
)

cleanupmongo

printheader 'And the answer is...'
echo "${ANSWER}"
