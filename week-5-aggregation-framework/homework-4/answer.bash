#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader 'Copying zip file.'
ZIP_NAME=chapter_5_aggregation_framework.f05059ebb3a5.zip
cp -v "../${ZIP_NAME}" .

printheader 'Unzipping the zip code data.'
ZIP_CODE_DATA_FILE="zips__code_data_sample_zip_52efe2cae2d423438505544e.json"
unzip "${ZIP_NAME}" "${ZIP_CODE_DATA_FILE}"

printheader 'Deleting zip file.'
rm -fv "${ZIP_NAME}"

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader 'Importing the zip code data.'
ZIP_CODE_DB_NAME=test
mongoimport --drop -d "${ZIP_CODE_DB_NAME}" -c zips "${ZIP_CODE_DATA_FILE}"

printheader 'Deleting the grade data file.'
rm -rfv "${ZIP_CODE_DATA_FILE}"

printheader "Ensuring data was imported correctly."
EXPECTED='29467'
ACTUAL=$(mongo --quiet "${ZIP_CODE_DB_NAME}" <<EOF
    db.zips.count()
EOF
)
assertequals "${EXPECTED}" "${ACTUAL}" "Number of zip code documented does not match"

printheader "Calculating population outside of cities."
ANSWER=$(mongo --quiet "${ZIP_CODE_DB_NAME}" <<EOF
    db.zips.aggregate([
        {"\$project": {
            _id: "\$_id",
            population: "\$pop",
            first_char: {"\$substr": ["\$city", 0, 1]},
        }},
        {"\$match": {"\$or": [
            {first_char: "0"},
            {first_char: "1"},
            {first_char: "2"},
            {first_char: "3"},
            {first_char: "4"},
            {first_char: "5"},
            {first_char: "6"},
            {first_char: "7"},
            {first_char: "8"},
            {first_char: "9"},
        ]}},
        {"\$group": {
            _id: null,
            population: {"\$sum": "\$population"},
        }},
    ])
EOF
)

cleanupmongo

printheader 'And the answer is...'
echo "${ANSWER}"
