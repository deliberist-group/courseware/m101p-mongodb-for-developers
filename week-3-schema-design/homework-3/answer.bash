#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader "Unzipping project"
BLOG_DIR=hw3-2and3-3
rm -rf "${BLOG_DIR}"
unzip blog-week-3-m101p.9b164043aad5.zip

printheader "Finding files that need to be patched."
pushd patches &>/dev/null
FILES="$(find . -type f)"
echo "${FILES}"
popd &>/dev/null

#WIDTH=$(($COLUMNS/2))
WIDTH=80
for file in "${FILES}"; do
    printheader "Printing diff of: ${file}"
    diff \
        --side-by-side \
        --suppress-common-lines \
        --ignore-all-space \
        "patches/${file}" \
        "${BLOG_DIR}/${file}"
done

printheader "Installing patched files"
cp -fv patches/*.py "${BLOG_DIR}"/

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader "Starting the blog."
pushd "${BLOG_DIR}" &>/dev/null
python blog.py &>blog.log &
sleep 10
popd &>/dev/null

printheader "Press [ENTER] to continue"
read

printheader "Validating changes."
pushd "${BLOG_DIR}" &>/dev/null
python validate.py
popd &>/dev/null

printheader "Shutting down the blog."
BLOG_PS_LINE="$(ps aux | grep blog.py | grep -v grep)"
POTENTIAL_PIDS=""
POTENTIAL_PIDS="${POTENTIAL_PIDS} $(echo "${BLOG_PS_LINE}" | cut -d" " -f2)"
POTENTIAL_PIDS="${POTENTIAL_PIDS} $(echo "${BLOG_PS_LINE}" | cut -d" " -f3)"
POTENTIAL_PIDS="${POTENTIAL_PIDS} $(echo "${BLOG_PS_LINE}" | cut -d" " -f4)"
kill ${POTENTIAL_PIDS}
sleep 2

cleanupmongo

printheader "Deleting project files."
rm -rf "${BLOG_DIR}"
