#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader "Importing data."
mongoimport --drop -d school -c students students.e7ed0a289cbe.json

printheader "Asserting data was imported correctly."
EXPECTED=200
ACTUAL=$(mongo --quiet localhost:27017/school <<EOF
    db.students.count()
EOF
)
assertequals "${EXPECTED}" "${ACTUAL}" "Number of students do not match"

printheader "Running script to remove the lowest homework."
python ./remove-lowest-homework.py

printheader "Asserting data was processed correctly."
EXPECTED='{ "_id" : 137, "name" : "Tamika Schildgen", "scores" : [ { "score" : 4.433956226109692, "type" : "exam" }, { "score" : 65.50313785402548, "type" : "quiz" }, { "score" : 89.5950384993947, "type" : "homework" } ] }'
ACTUAL=$(mongo --quiet localhost:27017/school <<EOF
    db.students.find({_id: 137})
EOF
)
assertequals "${EXPECTED}" "${ACTUAL}" "Student #137 is not as expected."

printheader "Determining answer, which will be the student_id."
ANSWER=$(mongo --quiet localhost:27017/school <<EOF
db.students.aggregate( [ \
      { '\$unwind': '\$scores' }, \
      { \
        '\$group': \
        { \
          '_id': '\$_id', \
          'average': { '\$avg': '\$scores.score' } \
        } \
      }, \
      { '\$sort': { 'average' : -1 } }, \
      { '\$limit': 1 } ] )
EOF
)
echo 'The answer is:' "${ANSWER}"

cleanupmongo
