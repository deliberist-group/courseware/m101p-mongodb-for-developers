#!/usr/bin/env python

from pymongo import MongoClient

connection = MongoClient("mongodb://localhost")
school = connection.school
students = school.students

docs = students.find()

for doc in docs:
    # Find the lowest homework score.
    lowest_score = 10000000000
    for assignment in doc['scores']:
        if assignment['type'] == "homework":
            if assignment['score'] < lowest_score:
                lowest_score = assignment['score']

    # Remove the lowest homework score from the document.
    for assignment in doc['scores']:
        if assignment['type'] == "homework":
            if assignment['score'] == lowest_score:
                doc['scores'].remove(assignment)

    # Update the document.
    students.update({"_id": doc['_id']}, doc)
