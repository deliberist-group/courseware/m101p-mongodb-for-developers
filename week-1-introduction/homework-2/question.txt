Download Handouts:

    hw1-2.py

Get PyMongo installed on your computer. To prove its installed, run the program:

    $ python hw1-2.py

Note that you will need to get MongoDB installed and the homework dataset
imported from the previous homework before attempting this problem.

This program will print a numeric answer. Please put just the 4-digit number
into the box below (no spaces).

CORRECT ANSWER INPUT:
    1815
