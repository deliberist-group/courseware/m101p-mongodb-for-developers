#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader "Setting up mongod."
mkdir ${DBPATH}
nohup mongod --dbpath ${DBPATH} &>/dev/null &

printheader "Unzipping pre-data."
unzip ../homework-1/hw1-1__m101j_m101p_5258458de2d4233537765336.e116b97d7133.zip

printheader "Restoring pre-data into mongod."
mongorestore dump

printheader "Running the homework script."
nohup python hw1-3.55b3698b9e47.py &>/dev/null &
sleep 4

printheader "CURLing the answer."
curl http://localhost:8080/hw1/50

printheader "Stopping the homework script."
HW_PY_SCRIPT_PID=$(ps aux | grep hw1-3 | grep -v grep | cut -d' ' -f3)
kill "${HW_PY_SCRIPT_PID}"
kill -9 "${HW_PY_SCRIPT_PID}"
sleep 4

printheader "Shutting down the database."
mongod --dbpath ${DBPATH} --shutdown
killall mongod

printheader "Deleting the database files."
rm -rf ${DBPATH}

printheader "Deleting the directory created by the zip."
rm -rf dump



