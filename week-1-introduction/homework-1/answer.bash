#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader "Setting up mongod."
mkdir ${DBPATH}
nohup mongod --dbpath ${DBPATH} &>/dev/null &

printheader "Unzipping pre-data."
unzip hw1-1__m101j_m101p_5258458de2d4233537765336.e116b97d7133.zip

printheader "Restoring pre-data into mongod."
mongorestore dump

printheader "Finding the answer to HW1."
mongo 127.0.0.1/m101 <<EOF
db.hw1.findOne()
EOF

printheader "Shutdown the database."
mongod --dbpath ${DBPATH} --shutdown
killall mongod

printheader "Deleting the database files."
rm -rf ${DBPATH}

printheader "Deleting the directory created by the zip."
rm -rf dump



