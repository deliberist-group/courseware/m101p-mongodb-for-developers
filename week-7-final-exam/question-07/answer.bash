#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader 'Extracting blog code and data.'
ZIP_FILE_NAME=final_exam.14345398f547.zip
PHOTO_DIR_NAME=final7__f7_m101_52e000fde2d423744501d031
ALBUM_FILE="${PHOTO_DIR_NAME}/final7/albums.json"
IMAGES_FILE="${PHOTO_DIR_NAME}/final7/images.json"
unzip "../${ZIP_FILE_NAME}" "${ALBUM_FILE}" "${IMAGES_FILE}"

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader 'Importing photo data.'
DATABASE_NAME=final7
ALBUM_COLLECTION_NAME=albums
IMAGES_COLLECTION_NAME=images
mongoimport \
    --drop \
    --db="${DATABASE_NAME}" \
    --collection="${ALBUM_COLLECTION_NAME}" \
    "${ALBUM_FILE}"
mongoimport \
    --drop \
    --db="${DATABASE_NAME}" \
    --collection="${IMAGES_COLLECTION_NAME}" \
    "${IMAGES_FILE}"
rm -rfv "${PHOTO_DIR_NAME}"

printheader 'Creating indexes to increase performance.'
mongo --quiet "${DATABASE_NAME}" <<EOF
    db.${ALBUM_COLLECTION_NAME}.createIndex({'images': 1})
    db.${IMAGES_COLLECTION_NAME}.createIndex({'tags': 1})
EOF

printheader 'Performing sanity check on number of documents tagging kittens.'
EXPECTED=49932
ACTUAL=$(mongo --quiet "${DATABASE_NAME}" <<EOF
    db.${IMAGES_COLLECTION_NAME}.count({'tags': 'kittens'})
EOF
)
assertequals "${EXPECTED}" "${ACTUAL}" "Number of kitten-tagged images do not match."

printheader 'Running orphan cleanup script.'
python ./cleanup-orphans.py

printheader 'Performing sanity check on images collection.'
EXPECTED=89737
ACTUAL=$(mongo --quiet "${DATABASE_NAME}" <<EOF
    db.${IMAGES_COLLECTION_NAME}.count()
EOF
)
assertequals "${EXPECTED}" "${ACTUAL}" "Number of image documents remaining do not match."

printheader 'Finding the answer.'
ANSWER=$(mongo --quiet "${DATABASE_NAME}" <<EOF
    db.${IMAGES_COLLECTION_NAME}.count({'tags': 'kittens'})
EOF
)

cleanupmongo

printheader 'And the answer is:'
echo "${ANSWER}"
