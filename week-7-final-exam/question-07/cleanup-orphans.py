#!/usr/bin/env python

from pymongo import MongoClient

connection = MongoClient("mongodb://localhost")
albums = connection.final7.albums
images = connection.final7.images

all_images = images.find()

num_deleted = 0

for doc in all_images:
    # Try to find album with that image in it.
    num_of_albums = albums.find({'images': doc['_id']}).count()

    # If the number of albums is '0', then the image is an orphan.
    if num_of_albums <= 0:
        #print('Deleting: {}'.format(doc))
        images.remove(doc)
        num_deleted += 1

print('Deleted {} documents.'.format(num_deleted))
