#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader 'Extracting blog code and data.'
ZIP_FILE_NAME=final_exam.14345398f547.zip
BLOG_CODE_DIR=final4__f4_m101p_52e01feae2d423744501d040
BLOG_DATA_FILE=posts__f4_m101j_m101js_m101p_52d94c86e2d423744501ce90.json
rm -rfv "${BLOG_CODE_DIR}"
unzip "../${ZIP_FILE_NAME}" "${BLOG_CODE_DIR}/*" "${BLOG_DATA_FILE}"

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader 'Importing blog data.'
BLOG_DATABASE_NAME=blog
BLOG_COLLECTION_NAME=posts
mongoimport \
    --drop \
    --db="${BLOG_DATABASE_NAME}" \
    --collection="${BLOG_COLLECTION_NAME}" \
    "${BLOG_DATA_FILE}"
rm -fv "${BLOG_DATA_FILE}"

printheader "Finding files that need to be patched."
PATCHES_DIR=patches
pushd "${PATCHES_DIR}" &>/dev/null
FILES="$(find . -type f)"
echo "${FILES}"
popd &>/dev/null

#WIDTH=$(($COLUMNS/2))
WIDTH=80
for file in ${FILES}; do
    printheader "Printing diff of: ${file}"
    diff \
        --side-by-side \
        --suppress-common-lines \
        --ignore-all-space \
        "${PATCHES_DIR}/${file}" \
        "${BLOG_CODE_DIR}/final4/${file}"
done

printheader "Installing patched files"
cp -fv ${PATCHES_DIR}/*.py "${BLOG_CODE_DIR}/final4/"

printheader "Starting the blog."
pushd "${BLOG_CODE_DIR}/final4" &>/dev/null
nohup python "blog.py" &>blog.log &
popd &>/dev/null
sleep 10s

printheader "Validating changes."
ANSWER=$(python "${BLOG_CODE_DIR}/final4/validate.py" 2>&1)

printheader "Shutting down the blog."
BLOG_PS_LINE="$(ps aux | grep blog.py | grep -v grep)"
POTENTIAL_PIDS=""
POTENTIAL_PIDS="${POTENTIAL_PIDS} $(echo "${BLOG_PS_LINE}" | cut -d" " -f2)"
POTENTIAL_PIDS="${POTENTIAL_PIDS} $(echo "${BLOG_PS_LINE}" | cut -d" " -f3)"
POTENTIAL_PIDS="${POTENTIAL_PIDS} $(echo "${BLOG_PS_LINE}" | cut -d" " -f4)"
kill ${POTENTIAL_PIDS}
sleep 2s

cleanupmongo

printheader 'Removing blog code.'
rm -rfv "${BLOG_CODE_DIR}"

printheader 'And the answer is:'
echo "${ANSWER}"
