#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader 'Setting up indexes.'
DB_NAME=test
mongo --quiet "${DB_NAME}" <<EOF
    db.stuff.createIndex({"a": 1, "b": 1})
    db.stuff.createIndex({"a": 1, "c": 1})
    db.stuff.createIndex({"c": 1})
    db.stuff.createIndex({"a": 1, "b": 1, "c": -1})
EOF

printheader 'Determining which indexes are used..'
mongo --quiet "${DB_NAME}" <<EOF
    db.stuff \
        .explain() \
        .find({'a':{'\$lt':10000}, 'b':{'\$gt': 5000}}, {'a':1, 'c':1}) \
        .sort({'c':-1})
EOF

cleanupmongo
