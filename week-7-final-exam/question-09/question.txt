Imagine an electronic medical record database designed to hold the medical
records of every individual in the United States. Because each person has more
than 16MB of medical history and records, it's not feasible to have a single
document for every patient. Instead, there is a patient collection that contains
basic information on each person and maps the person to a patient_id, and a
record collection that contains one document for each test or procedure. One
patient may have dozens or even hundreds of documents in the record collection.

We need to decide on a shard key to shard the record collection. What's the best
shard key for the record collection, provided that we are willing to run
inefficient scatter-gather operations to do infrequent research and run studies
on various diseases and cohorts? That is, think mostly about the operational
aspects of such a system. And by operational, we mean, think about what the most
common operations that this systems needs to perform day in and day out.

Choose the best answer:
[x] patient_id
[ ] _id
[ ] Primary care physician (your principal doctor that handles everyday problems)
[ ] Date and time when medical record was created
[ ] Patient first name
[ ] Patient last name

I say patient_id with the assumption that a patient_id is sufficiently random
and not monotonically increasing.  Sharding on patient_id allows for ALL of the
patient's records to be on the same shard, but since I'm assuming the patient_id
is sufficiently random then all patients will be evenly sharded across the
system.

By using _id you will be scattering all of the records for a single patient
across the system.  So for every time a doctor requests for a patient's medical
history it will have to gather data from every single shard in the cluster.

The Primary Care Physician just doesn't indicate to me there is enough entropy
in that field for even sharding.

The same goes for the patient's first or last name.

Lastly, the date and time of medical record creation is just a monotonically
increasing value.  In which case, we learned that fields that monotonically
increase, or change consistently, are poor choices for shard keys.  Because all
clients will route most CRUD operations to generally the same shard while other
shards in the cluster are sitting idle.
