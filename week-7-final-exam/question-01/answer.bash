#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader 'Unzipping Enron data.'
ZIP_NAME=enron.zip
unzip "${ZIP_NAME}"

printheader 'Restoring Enron data'
ENRON_DUMP_DIR=dump
mongorestore --drop "${ENRON_DUMP_DIR}"

printheader 'Deleting raw dump data.'
rm -rfv "${ENRON_DUMP_DIR}"

printheader "Finding emails from Andrew Fastow to John Lavorato."
ENRON_DB_NAME=enron
EXPECTED='1'
ACTUAL=$(mongo --quiet "${ENRON_DB_NAME}" <<EOF
    db.messages.find({
        'headers.From': {'\$regex': '.*fastow.*'},
        'headers.To': {'\$regex': '.*lavorato.*'},
    }).count()
EOF
)
assertequals "${EXPECTED}" "${ACTUAL}" "Number of emails do not match"

printheader "Finding emails from Andrew Fastow to Jeff Skilling."
ANSWER=$(mongo --quiet "${ENRON_DB_NAME}" <<EOF
    db.messages.find({
        'headers.From': {'\$regex': '.*fastow.*'},
        'headers.To': {'\$regex': '.*skilling.*'},
    }).count()
EOF
)

cleanupmongo

printheader 'And the answer is...'
echo "${ANSWER}"
