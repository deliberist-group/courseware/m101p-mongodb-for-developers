#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader 'Restoring Enron data'
ZIP_NAME=../question-01/enron.zip
ENRON_DUMP_DIR=dump
unzip "${ZIP_NAME}"
mongorestore --drop "${ENRON_DUMP_DIR}"
rm -rfv "${ENRON_DUMP_DIR}"

printheader "Adding Mr. Potatohead to an email."
ENRON_DB_NAME=enron
mongo --quiet "${ENRON_DB_NAME}" <<EOF
    db.messages.update(
        {'headers.Message-ID': '<8147308.1075851042335.JavaMail.evans@thyme>'},
        {'\$push': {'headers.To': 'mrpotatohead@mongodb.com'}}
    )
EOF

printheader 'Running validation script.'
FINAL_EXAM_ZIP=final_exam.14345398f547.zip
VALIDATION_SCRIPT=final3-validate-mongo-shell_56c3a13ad8ca391cef3abe58.js
unzip "../${FINAL_EXAM_ZIP}" "${VALIDATION_SCRIPT}"
ANSWER=$(mongo --quiet "${VALIDATION_SCRIPT}")
rm -fv "${VALIDATION_SCRIPT}"

cleanupmongo

printheader 'And the answer is...'
echo "${ANSWER}"
