#!/usr/bin/env python

from pymongo import MongoClient
from pymongo import ASCENDING

connection = MongoClient("mongodb://localhost")
students = connection.students
grades = students.grades

docs = grades\
    .find({'type': 'homework'})\
    .sort([('student_id', ASCENDING),
           ('score', ASCENDING)
           ])

previous_student_id = None
for doc in docs:
    try:
        if doc['student_id'] != previous_student_id:
            grades.delete_one(doc)
    finally:
        previous_student_id = doc['student_id']
