#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader "Unzipping movie database"
rm -rf dump
unzip dump.zip

printheader "Importing movie database."
mongorestore dump

printheader "Determining the best moving (i.e. THE ANSWER!!!!)."
mongo --quiet localhost:27017/video <<EOF
    db.movieDetails.find( \
        { \
            "countries.1": "Sweden", \
        }) \
        .count()
EOF

printheader "Shutting down the database."
mongod --dbpath ${DBPATH} --shutdown
killall mongod

printheader "Deleting the database files."
rm -rf ${DBPATH}

printheader "Deleting project files."
rm -rf dump

