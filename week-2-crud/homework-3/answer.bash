#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
cd "${SCRIPT_DIR}"
source ../../common-project-env.bash

printheader "Unzipping project"
BLOG_DIR=login_logout_signup
rm -rf "${BLOG_DIR}"
unzip login_logout_signup.56215002a122.zip

printheader "Finding files that need to be patched."
pushd patches &>/dev/null
FILES="$(find . -type f)"
popd &>/dev/null

#WIDTH=$(($COLUMNS/2))
WIDTH=80
for file in ${FILES}; do
    printheader "Printing diff of: ${file}"
    diff \
        --side-by-side \
        --suppress-common-lines \
        --ignore-all-space \
        "patches/${file}" \
        ""${BLOG_DIR}"/${file}"
done

printheader "Installing patched files"
cp -fv patches/*.py "${BLOG_DIR}/"

printheader "Setting up mongod."
mkdir ${DBPATH}
mongod --fork --logpath ${DBPATH}/mongod.log --dbpath ${DBPATH}

printheader "Starting the blog."
pushd "${BLOG_DIR}" &>/dev/null
nohup python blog.py &>blog.log &
sleep 10
popd &>/dev/null

printheader "Validating changes."
pushd "${BLOG_DIR}" &>/dev/null
python validate.py
popd &>/dev/null

printheader "Shutting down the blog."
BLOG_PS_LINE="$(ps aux | grep blog.py | grep -v grep)"
POTENTIAL_PIDS=""
POTENTIAL_PIDS="${POTENTIAL_PIDS} $(echo "${BLOG_PS_LINE}" | cut -d" " -f2)"
POTENTIAL_PIDS="${POTENTIAL_PIDS} $(echo "${BLOG_PS_LINE}" | cut -d" " -f3)"
POTENTIAL_PIDS="${POTENTIAL_PIDS} $(echo "${BLOG_PS_LINE}" | cut -d" " -f4)"
kill ${POTENTIAL_PIDS}
sleep 2

printheader "Shutting down the database."
mongod --dbpath ${DBPATH} --shutdown
killall mongod

printheader "Deleting the database files."
rm -rf ${DBPATH}

printheader "Deleting project files."
rm -rf "${BLOG_DIR}"

